from setuptools import setup, find_packages


def get_install_requires():
    with open("requirements/run") as descriptor:
        return list([
            dependency.strip("\n") for dependency in descriptor.readlines()])


def get_long_description():
    with open("README.md", "r") as descriptor:
        long_description = descriptor.read()
    return long_description


setup(
    name='ffx-customization-tool',
    version='1.0.0',
    description='FFX Customization Weapon/Armor Tool',
    author='Santiago Blanco Ventas',
    author_email="sblancov84@gmail.com",
    license='MIT',
    project_urls={
        'Source': 'https://gitlab.com/sblancov84/ffx-customization-tool'
    },
    python_requires='>=3.7',
    packages=find_packages(exclude=['docs']),
    package_data={'ffx': ['data/**/*.txt']},
    install_requires=get_install_requires(),
    entry_points={
        'console_scripts': ['ffx-customization-tool = ffx.main:main']
    },
    include_package_data=True,
    long_description=get_long_description(),
    long_description_content_type="text/markdown",
)
