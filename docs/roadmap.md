# Roadmap

- [x] Create local pipeline.
- [x] Create basic documentation for this project.
- [x] Create package process to distribute this tool.
- [x] Create publish process to upload this tool to PyPI.
- [ ] Create long description to show a better presentation into PyPI.
- [ ] Add some credits to FAQs authors from where data is extracted.
- [ ] Add some unit tests.
