# Developer Guide

## Linting

Flake8 and prospector has to be passed for every commit.

# Testing

## Unit tests

All unit tests has to be passed for every commit.

## Acceptance tests

All acceptance tests has to be passed for every commit.

# Contributing

## New features and Bugs

In order to develop new features just check [Roadmap](roadmap.md). Bugs are
listed in [Issues](issues.md)

Merge Request must comes from master branch and will merge into master branch.
