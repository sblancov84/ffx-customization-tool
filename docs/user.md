# User Guide

## How to use

> Before using this tool you need to install it in your system. For more
  details, please consult the [Administrator Guide](./administrator.md).

Just invoke the next command in your favorite command line:

    ffx-customization-tool

Then a screen like this will appear:

![ffx-customization-tool](./images/ffx-customization-tool.gif)

I am going to describe what happend in the gif, although nobody will read it :)

A menu will appear and you have to choose between Armor or Weapon, use
your arrow keys to move and 'Enter' to select one of them.

The next step is to select a customization. You can move up and down with arrow
keys or search for anyone typing '/' and then start to type the customization
you are looking for, and select what you want using arrow keys and 'Enter'
again to view which monsters allow you to get material you need.

Then the application is finished.
