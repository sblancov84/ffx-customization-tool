# Administrator Guide

## Installing

As any other python package:

    pip install ffx-customization-tool

## Configuring

This tool does not need configuration. Easy.

## Uninstalling

As any other python package:

    pip uninstall ffx-customization-tool
