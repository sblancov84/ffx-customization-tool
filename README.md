# FFX Customization Tool

## Introduction

The aim of this tool is make easier to find the source of material to add a
customization into a Weapon or Armor in Final Fantasy X.

## For administrators

Please, read [administrator manual](docs/administrator.md). There you can find
instructions about how to install, configure and uninstall this tool.

## For users

Please, read [user manual](docs/user.md). There you can find instructions about
how to use this tool.

# For developers

Please, read [developer manual](docs/developer.md). There you can find
instructions about how to develop this tool.

# References

There are some sources what I have to read to get data, so credit for the
authors too. See details into [references](docs/references.md)
