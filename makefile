# venv-create:
# 	pyenv virtualenv 3.7.3 ffx

docker/build:
	docker build -t ffx-customization-tool:dev-1.0 \
		-f environments/development/Dockerfile .

docker/run:
	docker run --rm -it -v ${PWD}:/app/ \
		ffx-customization-tool:dev-1.0 python ffx/main.py

docker/remove-image:
	docker image rm ffx-customization-tool:dev-1.0

flake8:
	docker run --rm -it -v ${PWD}:/app/ \
		ffx-customization-tool:dev-1.0 flake8 ffx/ tests/

prospector:
	docker run --rm -it -v ${PWD}:/app/ \
		ffx-customization-tool:dev-1.0 prospector ffx/

precommit: flake8 prospector

package:
	docker run --rm -it -v ${PWD}:/app/ \
		ffx-customization-tool:dev-1.0 python setup.py bdist_wheel

clean:
	docker run --rm -it -v ${PWD}:/app/ \
		ffx-customization-tool:dev-1.0 scripts/clean.sh

install:
	pip install dist/ffx_customization_tool*

uninstall:
	pip uninstall dist/ffx_customization_tool*

run-cli:
	ffx-customization-tool

easy-loop: uninstall clean package install

publish:
	twine upload dist/*

unit-test:
	docker run --rm -it -v ${PWD}:/app/ \
		ffx-customization-tool:dev-1.0 pytest -v

tdd:
	docker run --rm -it -v ${PWD}:/app/ \
		ffx-customization-tool:dev-1.0 ptw -- -v -m current
