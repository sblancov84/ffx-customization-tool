#!/usr/bin/env bash

rm -rf ffx_customization_tool.egg-info build dist $(find . -name __pycache__)
