

class AbilityParser:
    """
    Ability: Fire Ward
    Effect: Halves fire damage; includes spells and regular attacks.
    Requires: Bomb Fragment x4
    """

    def parse(self, raw):
        pass


class RequirementParser:
    """
    Requires: Bomb Fragment x4
    """

    def parse(self, raw):
        result = raw.rsplit(" ", 1)
        return {
            "item": result[0],
            "quantity": int(result[1].replace("x", "")),
        }
