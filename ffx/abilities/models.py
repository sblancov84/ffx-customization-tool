from dataclasses import dataclass
from typing import Type


@dataclass
class Requirement:
    item: str
    quantity: int

    def __repr__(self):
        return repr({
            "item": self.item,
            "quantity": self.quantity,
        })


@dataclass
class Ability:
    name: str
    effect: str
    requirement: Type[Requirement]

    # This is necessary because we want to represent requirement as dictionary
    def __repr__(self):
        return repr({
            "name": self.name,
            "effect": self.effect,
            "requirement": self.requirement,
        })
