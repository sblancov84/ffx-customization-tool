import logging


class UnspectedStealFormat(Exception):
    pass


class StealParser:

    def parse(self, raw):
        try:
            return self.__parse(raw)
        except Exception as exception:
            field_name = self.__class__.__name__.replace("Parser", "").lower()
            message = f"{field_name} field cannot be parsed: {raw}"
            logging.error(exception)
            raise UnspectedStealFormat(f"{message}\n{exception}")

    def __parse(self, raw):
        result = raw.split(",")
        if len(result) == 1:
            common = rare = result[0].strip()
        elif len(result) == 2:
            if "both common" in result[1]:
                common = ",".join(result).replace("(both common)", "").strip()
                rare = ""
            else:
                common = result[0].replace("(common)", "").strip()
                rare = result[1].replace("(rare)", "").strip()
        return {
            "common": common,
            "rare": rare
        }


class DropParser(StealParser):
    pass
