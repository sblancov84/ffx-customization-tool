"""
example = {
    "Auto Phoenix": {
        "material": {
            "name": "MegaPhoenix",
            "quantity": 30
        },
        "sources": [{
            "fiend": "Defender X",
            "location": "Zanarkand",
            "mode": {
                "theft": "Phoenix (common), MegaPhoenix (rare)",
                "drop": "Phoenix x2 (common), MegaPhoenix x2 (rare)",
                "bribery": "MegaPhoenix x2 for 1,600,000",
            },
        }]
    }
}
"""


class Item:

    def __init__(self):
        pass

    def parse(self):
        pass
