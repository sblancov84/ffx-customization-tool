import pytest

from ffx.monsters.models import Steal, Bribe, Monster, Drop

"""
 Name: Piranha                    Location: Salvage Ship, Besaid, Island Lagoon
Steal: Grenade
Bribe: Water Gem x1 for 1,000 gil
 Drop: Power Sphere (common), Ability Sphere (rare)
"""


class TestSteal:

    def test_repr_ok(self):
        common = "Grenade"
        rare = "Grenade"
        steal = Steal(common, rare)
        current = repr(steal)
        expected = "{'common': 'Grenade', 'rare': 'Grenade'}"
        assert current == expected

    @pytest.mark.parametrize("field", ["common", "rare"])
    def test_fields_ok(self, field):
        common = "Grenade"
        rare = "Grenade"
        steal = Steal(common, rare)
        current = getattr(steal, field)
        expected = locals()[field]
        assert current == expected


class TestDrop:

    def test_repr_ok(self):
        common = "Power Sphere"
        rare = "Ability Sphere"
        drop = Drop(common, rare)
        current = repr(drop)
        expected = "{'common': 'Power Sphere', 'rare': 'Ability Sphere'}"
        assert current == expected

    @pytest.mark.parametrize("field", ["common", "rare"])
    def test_fields_ok(self, field):
        common = "Power Sphere"
        rare = "Ability Sphere"
        drop = Drop(common, rare)
        current = getattr(drop, field)
        expected = locals()[field]
        assert current == expected


class TestBribe:

    def test_repr_ok(self):
        item = "Water Gem"
        quantity = 1
        gil = 1000
        bribe = Bribe(item, quantity, gil)
        current = repr(bribe)
        expected = "{'item': 'Water Gem', 'quantity': 1, 'gil': 1000}"
        assert current == expected

    @pytest.mark.parametrize("field", ["item", "quantity", "gil"])
    def test_fields_ok(self, field):
        item = "Water Gem"
        quantity = 1
        gil = 1000
        bribe = Bribe(item, quantity, gil)
        current = getattr(bribe, field)
        expected = locals()[field]
        assert current == expected


class TestMonster:

    def test_repr_ok(self):
        name = "Piranha"
        location = ["Salvage Ship", "Besaid", "Island Lagoon"]
        steal = Steal("Grenade", "Grenade")
        drop = Steal("Power Sphere", "Ability Sphere")
        bribe = Bribe("Water Gem", 1, 1000)
        monster = Monster(name, location, steal, drop, bribe)
        current = repr(monster)
        expected = "{'name': 'Piranha', 'location': ['Salvage Ship', " + \
            "'Besaid', 'Island Lagoon'], 'steal': {'common': 'Grenade', " + \
            "'rare': 'Grenade'}, 'drop': {'common': 'Power Sphere', " + \
            "'rare': 'Ability Sphere'}, 'bribe': {'item': 'Water Gem', " + \
            "'quantity': 1, 'gil': 1000}}"
        assert current == expected

    @pytest.mark.parametrize(
        "field", ["name", "location", "steal", "drop", "bribe"])
    def test_fields_ok(self, field):
        name = "Piranha"
        location = ["Salvage Ship", "Besaid", "Island Lagoon"]
        steal = Steal("Grenade", "Grenade")
        drop = Steal("Power Sphere", "Ability Sphere")
        bribe = Bribe("Water Gem", 1, 1000)
        monster = Monster(name, location, steal, drop, bribe)
        current = getattr(monster, field)
        expected = locals()[field]
        assert current == expected
