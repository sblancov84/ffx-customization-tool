import pytest

from ffx.monsters.parsers import StealParser, UnspectedStealFormat


class TestStealParser:

    def test_parse_simple(self):
        raw = "Grenade"
        parser = StealParser()
        current = parser.parse(raw)
        expected = {
            "common": raw,
            "rare": raw,
        }

        assert current == expected

    def test_parse_complex(self):
        raw = "Potion (common), Sleeping Powder (rare)"
        parser = StealParser()
        current = parser.parse(raw)
        expected = {
            "common": "Potion",
            "rare": "Sleeping Powder",
        }

        assert current == expected

    def test_parse_complex_wrong_no_common_no_rare(self):
        raw = "Potion, Sleeping Powder"
        parser = StealParser()
        current = parser.parse(raw)
        expected = {
            "common": "Potion",
            "rare": "Sleeping Powder",
        }

        assert current == expected

    def test_parse_complex_both_common(self):
        raw = " Power Sphere, Al Bhed Potion (both common)"
        parser = StealParser()
        current = parser.parse(raw)
        expected = {
            "common": "Power Sphere, Al Bhed Potion",
            "rare": "",
        }

        assert current == expected

    def test_parse_raise_exception_for_more_items(self):
        raw = " Power Sphere, Al Bhed Potion (both common), hasd"
        parser = StealParser()
        with pytest.raises(UnspectedStealFormat):
            parser.parse(raw)
