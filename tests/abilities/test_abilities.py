from ffx.abilities.parsers import RequirementParser


class TestRequimentParser:

    def test_parse(self):
        raw = "Bomb Fragment x4"
        parser = RequirementParser()
        current = parser.parse(raw)
        expected = {
            "item": "Bomb Fragment",
            "quantity": 4,
        }
        assert current == expected
