import pytest

from ffx.abilities.models import Requirement, Ability

"""
 Ability: Fire Ward
  Effect: Halves fire damage; includes spells and regular attacks.
Requires: Bomb Fragment x4
"""


class TestRequirement:

    def test_repr_ok(self):
        item = "Bomb Fragment"
        quantity = 4
        requirement = Requirement(item, quantity)
        current = repr(requirement)
        expected = "{'item': 'Bomb Fragment', 'quantity': 4}"
        assert current == expected

    @pytest.mark.parametrize("field", ["item", "quantity"])
    def test_fields_ok(self, field):
        item = "Bomb Fragment"
        quantity = 4
        requirement = Requirement(item, quantity)
        current = getattr(requirement, field)
        expected = locals()[field]
        assert current == expected


class TestAbility:

    def test_repr_ok(self):
        item = "name"
        quantity = 4
        requirement = Requirement(item, quantity)

        name = "Fire Ward"
        effect = "Halves fire damage; includes spells and regular attacks."

        ability = Ability(name, effect, requirement)

        current = repr(ability)
        expected = "{'name': 'Fire Ward', 'effect': 'Halves fire damage; " + \
            "includes spells and regular attacks.', 'requirement': " + \
            "{'item': 'name', 'quantity': 4}}"

        assert current == expected

    @pytest.mark.parametrize("field", ["name", "effect", "requirement"])
    def test_fields_ok(self, field):
        item = "name"
        quantity = 4
        requirement = Requirement(item, quantity)
        name = "Fire Ward"
        effect = "Halves fire damage; includes spells and regular attacks."
        ability = Ability(name, effect, requirement)

        current = getattr(ability, field)
        expected = locals()[field]
        assert current == expected
